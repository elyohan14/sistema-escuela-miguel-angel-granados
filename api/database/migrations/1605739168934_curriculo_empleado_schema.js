'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CurriculoEmpleadoSchema extends Schema {
  up () {
    this.create('curriculo_empleados', (table) => {
      table.increments()
      table.integer('empleado_id')
      table.integer('titulo_obtenido')
      table.string('nombre_titulo')
      table.string('descripcion_titulo')
      table.integer('annio_obtencion')
      table.string('institucion')
      table.timestamps()
    })
  }

  down () {
    this.drop('curriculo_empleados')
  }
}

module.exports = CurriculoEmpleadoSchema
