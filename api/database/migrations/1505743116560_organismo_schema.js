'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class OrganismoSchema extends Schema {
  up () {
    this.create('organismos', (table) => {
      table.increments()
      table.string('nombre')
      table.timestamps()
    })
  }

  down () {
    this.drop('organismos')
  }
}

module.exports = OrganismoSchema
