'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DatosLaboralesSchema extends Schema {
  up () {
    this.create('datos_laborales', (table) => {
      table.increments()
      table.integer('empleado_id')
      table.integer('plantel_id')
      // table.integer('dependencia_id') // Creo que no hace falta
      table.integer('organismo_id')
      table.integer('tipo_personal_id')
      table.string('fecha_ingreso_mpppe')
      table.string('cargo')
      table.string('codigo_cargo')
      table.string('funcion_real_cargo')
      table.string('codigo_dependencia_recibo_pago')
      table.string('ubicacion_fisica')
      table.integer('estatus_laboral_id')
      table.integer('cargo_segun_voucher')
      table.integer('funcion_actual')
      // el 
      table.timestamps()
    })
  }

  down () {
    this.drop('datos_laborales')
  }
}

module.exports = DatosLaboralesSchema
