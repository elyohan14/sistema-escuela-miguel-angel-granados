'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class EstudianteSchema extends Schema {
  up () {
    this.create('estudiantes', (table) => {
      table.increments()
      table.string('nombre', 80).notNullable()
      table.string('apellido', 80).notNullable()
      table.string('cedula', 80)
      table.string('email', 80)
      table.string('telefono')
      table.timestamps()
    })
  }

  down () {
    this.drop('estudiantes')
  }
}

module.exports = EstudianteSchema
