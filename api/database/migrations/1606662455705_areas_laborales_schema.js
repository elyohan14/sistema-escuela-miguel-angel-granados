'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AreasLaboralesSchema extends Schema {
  up () {
    this.create('areas_laborales', (table) => {
      table.increments()
      table.string('nombre')
      table.timestamps()
    })
  }

  down () {
    this.drop('areas_laborales')
  }
}

module.exports = AreasLaboralesSchema
