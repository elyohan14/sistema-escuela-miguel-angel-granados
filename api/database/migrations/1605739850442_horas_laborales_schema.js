'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class HorasLaboralesSchema extends Schema {
  up () {
    this.create('horas_laborales', (table) => {
      table.increments()
      table.integer('empleado_id').references('id').inTable('empleados').unsigned()
      table.integer('horas_administrativas')
      table.integer('horas_docentes')
      table.integer('total')
      table.timestamps()
    })
  }

  down () {
    this.drop('horas_laborales')
  }
}

module.exports = HorasLaboralesSchema
