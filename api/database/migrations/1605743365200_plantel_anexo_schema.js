'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PlantelAnexoSchema extends Schema {
  up () {
    this.create('plantel_anexos', (table) => {
      table.increments()
      table.string('codigo')
      table.string('codigo_electoral')
      table.string('codigo_circuito')
      table.string('nombre')
      table.string('direccion')
      table.string('municipio')
      table.string('telefono')
      table.timestamps()
    })
  }

  down () {
    this.drop('plantel_anexos')
  }
}

module.exports = PlantelAnexoSchema
