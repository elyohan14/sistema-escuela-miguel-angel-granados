'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AreasLaboralesSchema extends Schema {
  up () {
    this.create('areas_laborales_empleados', (table) => {
      table.increments()
      table.integer('empleado_id').references('id').inTable('empleados').unsigned()
      table.integer('area_laboral_id')
      table.integer('cantidad')
      table.timestamps()
    })
  }

  down () {
    this.drop('areas_laborales_empleados')
  }
}

module.exports = AreasLaboralesSchema
