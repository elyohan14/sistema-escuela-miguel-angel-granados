'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RepresentanteSchema extends Schema {
  up () {
    this.create('representantes', (table) => {
      table.increments()
      table.string('nombre', 80).notNullable()
      table.string('apellido', 80).notNullable()
      table.string('cedula', 80).notNullable()
      table.string('telefono', 80).notNullable()
      table.string('direccion', 80).notNullable()
      table.string('email', 80).notNullable()
      
      table.timestamps()
    })
  }

  down () {
    this.drop('representantes')
  }
}

module.exports = RepresentanteSchema
