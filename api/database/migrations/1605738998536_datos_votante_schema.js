'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DatosVotanteSchema extends Schema {
  up () {
    this.create('datos_votantes', (table) => {
      table.increments()
      table.integer('empleado_id')
      table.string('centro_votacion')
      table.string('direccion_centro_votacion')
      table.timestamps()
    })
  }

  down () {
    this.drop('datos_votantes')
  }
}

module.exports = DatosVotanteSchema
