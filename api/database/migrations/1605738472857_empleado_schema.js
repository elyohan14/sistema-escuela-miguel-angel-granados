'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class EmpleadoSchema extends Schema {
  up () {
    this.create('empleados', (table) => {
      table.increments()
      table.integer('plantel_id').references('id').inTable('planteles').unsigned()
      table.integer('tipo_empleado')
      table.string('cedula')
      table.string('nombres')
      table.string('apellidos')
      table.date('fecha_nacimiento')
      table.enu('sexo', ['M', 'F'])
      table.string('telefono')
      table.string('correo')
      table.string('twitter')
      table.string('direccion')
      table.integer('municipio')
      table.timestamps()
    })
  }

  down () {
    this.drop('empleados')
  }
}

module.exports = EmpleadoSchema
