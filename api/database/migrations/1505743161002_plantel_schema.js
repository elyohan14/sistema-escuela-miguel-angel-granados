'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PlantelSchema extends Schema {
  up () {
    this.create('planteles', (table) => {
      table.increments()
      table.string('codigo_dependencia')
      table.string('codigo_dea')
      table.string('codigo_estadistico')
      table.string('codigo_electoral')
      table.string('rif')
      table.string('codigo_circuito')
      table.integer('turno_id')
      table.integer('tipo_matricula_id')
      table.boolean('bolivariana')
      table.string('nombre')
      table.string('direccion')
      table.string('municipio')
      table.integer('denominacion_id')
      table.integer('dependencia_id')
      table.string('niveles')
      table.integer('ubicacion_id')
      table.string('correo')
      table.string('telefono')
      table.string('estatus_id')
      table.string('planteles_anexos')
      table.timestamps()
    })
  }

  down () {
    this.drop('planteles')
  }
}

module.exports = PlantelSchema
