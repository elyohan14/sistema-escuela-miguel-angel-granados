'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class DatosLaborale extends Model {
  organismo () {
    return this.belongsTo('App/Models/Organismo', 'organismo_id', 'id')
  }
}

module.exports = DatosLaborale
