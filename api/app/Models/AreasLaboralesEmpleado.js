'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class AreasLaboralesEmpleados extends Model {
  nombre () {
    return this.belongsTo('App/Models/AreasLaborale', 'area_laboral_id', 'id')
  }
}

module.exports = AreasLaboralesEmpleados
