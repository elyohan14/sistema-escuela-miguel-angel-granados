'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Empleado extends Model {
  datosLaborales () {
    return this.hasOne('App/Models/DatosLaborale', 'id', 'empleado_id')
  }
  curriculo () {
    return this.hasOne('App/Models/CurriculoEmpleado', 'id', 'empleado_id')
  }
  datosElectorales () {
    return this.hasOne('App/Models/DatosVotante', 'id', 'empleado_id')
  }
  areasLaborales () {
    return this.hasMany('App/Models/AreasLaboralesEmpleado', 'id', 'empleado_id')
  }
}

module.exports = Empleado
