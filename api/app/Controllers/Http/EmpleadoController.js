'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with empleados
 */
const moment =  require('moment')
const Empleado = use('App/Models/Empleado')
const DatosLaborales = use('App/Models/DatosLaborale')
const DatosVotante = use('App/Models/DatosVotante')
const CurriculoEmpleado = use('App/Models/CurriculoEmpleado')
const AreasLaboralesEmpleados = use('App/Models/AreasLaboralesEmpleado')
const ExcelJS = require('exceljs')
const Helpers = use("Helpers")
class EmpleadoController {
  /**
   * Show a list of all empleados.
   * GET empleados
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
    const empleados = await Empleado.all()
    response.send(empleados)
  }

  /**
   * Create/save a new empleado.
   * POST empleados
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    const req = request.all()
    const datosPersonales = req.datos_personales
    const datosLaborales = req.datos_laborales
    const datosElectorales = req.datos_electorales
    const curriculo = req.curriculo
    
    const areasLaborales = req.datos_laborales.areasLaborales
    delete req.datos_laborales.areasLaborales
    const empleado = await Empleado.create(datosPersonales)
    datosLaborales.empleado_id = empleado.id
    datosElectorales.empleado_id = empleado.id
    curriculo.empleado_id = empleado.id
    await DatosLaborales.create(datosLaborales)
    await DatosVotante.create(datosElectorales)
    await CurriculoEmpleado.create(curriculo)

    // Eliminar las áreas anteriores que tenía el empleado e insertarles las nuevas
    // await AreasLaboralesEmpleados.query().where('empleado_id', empleado.id).delete() en modificar

    for(const area of areasLaborales) {
      await AreasLaboralesEmpleados.create({
        empleado_id: empleado.id,
        area_laboral_id: area.area_laboral_id,
        cantidad: area.cantidad
      })
    }
    return areasLaborales
    return req
    response.send(empleado)
  }

  /**
   * Display a single empleado.
   * GET empleados/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
    const datos_personales = (await Empleado.query().with('datosLaborales')
    .with('curriculo').with('datosElectorales').with('areasLaborales.nombre')
    .where('id', params.id).first()).toJSON()
    datos_personales.fecha_nacimiento = moment(datos_personales.fecha_nacimiento).format('YYYY-MM-DD')
    datos_personales.datosLaborales.areasLaborales = datos_personales.areasLaborales

    datos_personales.areasLaborales.forEach(element => {
      element.nombre = element.nombre.nombre
    })
    const res = {
      datos_personales: datos_personales,
      datos_laborales: datos_personales.datosLaborales,
      curriculo: datos_personales.curriculo,
      datos_electorales: datos_personales.datosElectorales
    }
    response.send(res)
  }

  /**
   * Update empleado details.
   * PUT or PATCH empleados/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    const req = request.all()

    delete req.datos_personales.datosLaborales
    delete req.datos_personales.datosElectorales
    delete req.datos_personales.curriculo
    const datosPersonales = req.datos_personales
    const datosLaborales = req.datos_laborales
    const datosElectorales = req.datos_electorales
    const curriculo = req.curriculo
    const areasLaborales = req.datos_laborales.areasLaborales
    delete req.datos_laborales.areasLaborales
    delete req.datos_personales.areasLaborales

    
    const record = await Empleado.query().where('id', params.id).update(datosPersonales)

    
    datosLaborales.empleado_id = params.id
    datosElectorales.empleado_id = params.id
    curriculo.empleado_id = params.id
    await DatosLaborales.query().where('id', params.id).update(datosLaborales)
    await DatosVotante.query().where('id', params.id).update(datosElectorales)
    await CurriculoEmpleado.query().where('id', params.id).update(curriculo)

    // Eliminar las áreas anteriores que tenía el empleado e insertarles las nuevas
    await AreasLaboralesEmpleados.query().where('empleado_id', params.id).delete()

    for(const area of areasLaborales) {
      await AreasLaboralesEmpleados.create({
        empleado_id: params.id,
        area_laboral_id: area.area_laboral_id,
        cantidad: area.cantidad
      })
    }

    response.send(record)
  }

  /**
   * Delete a empleado with id.
   * DELETE empleados/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
    await Empleado.query().where('id', params.id).delete()
    response.send(true)
  }

  async imprimirReporte1 ({ response }) {
    const empleados = (await Empleado.query().with('datosLaborales.organismo').with('curriculo')
    .with('datosElectorales')
    .fetch()).toJSON()
    console.log('empleados', empleados)

    let workbook = new ExcelJS.Workbook()
    let worksheet = workbook.addWorksheet("Tutorials")

    worksheet.columns = [
      { header: "#", key: "id", width: 5 },
      { header: "Cédula", key: "cedula", width: 15 },
      { header: "Nombre y Apellido", key: "nombres", width: 25 },
      { header: "Fecha de nacimiento", key: "fecha_nacimiento", width: 15 },
      { header: "Organismo", key: "organismo", width: 10 },
      { header: "Titular", key: "titular", width: 10 },
      { header: "Contratado", key: "contratado", width: 10 },
      { header: "Fecha de ingreso al MPPE", key: "fecha_ingreso_mppe", width: 15 },
      { header: "Cargo", key: "cargo", width: 25},
      { header: "Función real que cumple", key: "funcion_real_cargo", width: 20 },
      /* { header: "Docente en especialidad", key: "published", width: 10 }, */
      { header: "TSU", key: "tsu", width: 10 },
      { header: "LCDO", key: "licenciado", width: 10 },
      { header: "Posgrado/Especialización", key: "posgrado", width: 10 },
      { header: "MSG", key: "msg", width: 10 },
      { header: "DR", key: "dr", width: 10 },
      { header: "Descripción del título", key: "descripcion_titulo", width: 25 },
      { header: "Teléfono", key: "telefono", width: 15 },
      { header: "Correo electrónico", key: "correo", width: 30 },
      { header: "Twitter", key: "twitter", width: 10 },
      { header: "Municipio", key: "municipio", width: 10 },
      { header: "Direccion", key: "direccion", width: 50 },
      { header: "Activo", key: "activo", width: 10 },
      { header: "Vacaciones", key: "vacaciones", width: 10 },
      { header: "Permiso contractual", key: "permiso", width: 20 },
      { header: "Reposo continuo", key: "reposo", width: 20 },
      { header: "Proceso de jubilación", key: "jubilacion", width: 20 },
      { header: "Centro de votación", key: "centro_votacion", width: 20 },
      { header: "Dirección del centro de votación", key: "direccion_centro", width: 50 },
    ]

    const records = []
    let i = 1
    empleados.forEach(element => {
      let tsu
      let licenciado
      let posgrado
      let msg
      let dr
      if (element.curriculo.titulo_obtenido == 1) {
        tsu = 'X'
      } else if (element.curriculo.titulo_obtenido == 2) {
        licenciado = 'X'
      } else if (element.curriculo.titulo_obtenido == 3) {
        posgrado = 'X'
      } else if (element.curriculo.titulo_obtenido == 4) {
        msg = 'X'
      } else if (element.curriculo.titulo_obtenido == 5) {
        msg = 'dr'
      }
      let activo, vacaciones, permiso, reposo, jubilacion

      if (element.datosLaborales.estatus_laboral_id == 1) {
        activo = 'X'
      } else if (element.datosLaborales.estatus_laboral_id == 2) {
        vacaciones = 'X'
      } else if (element.datosLaborales.estatus_laboral_id == 3) {
        permiso = 'X'
      } else if (element.datosLaborales.estatus_laboral_id == 4) {
        reposo = 'X'
      } else if (element.datosLaborales.estatus_laboral_id == 5) {
        jubilacion = 'X'
      }

      records.push({
        id: i,
        cedula: element.cedula,
        nombres: `${element.nombres} ${element.apellidos}`,
        fecha_nacimiento: moment(element.fecha_nacimiento).format('DD/MM/YYYY'),
        organismo: element.datosLaborales.organismo.nombre,
        titular: element.datosLaborales.tipo_personal_id == 1 ? 'X' : '',
        contratado: element.datosLaborales.tipo_personal_id == 2 ? 'X' : '',
        fecha_ingreso_mppe: moment(element.datosLaborales.fecha_ingreso_mpppe).format('DD/MM/YYYY'),
        cargo: element.datosLaborales.cargo,
        funcion_real_cargo: element.datosLaborales.funcion_real_cargo,
        tsu,
        licenciado,
        posgrado,
        descripcion_titulo: element.curriculo.descripcion_titulo,
        telefono: element.telefono,
        correo: element.correo,
        twitter: element.twitter,
        municipio: 'Nirgua',
        direccion: element.direccion,
        activo,
        vacaciones,
        permiso,
        reposo,
        jubilacion,
        centro_votacion: element.datosElectorales.centro_votacion,
        direccion_centro: element.datosElectorales.direccion_centro
      })
      i++
    })

    const tutorials = [
      {
        id: 1,
        published: 'Yohan'
      }
    ]

    // Add Array Rows
    worksheet.addRows(records);


    await workbook.xlsx.writeFile(Helpers.appRoot('Planificacion.xlsx'))

    response.download(Helpers.appRoot('Planificacion.xlsx'))
  }
}

module.exports = EmpleadoController
