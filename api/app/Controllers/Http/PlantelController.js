'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with plantels
 */
const Plantel = use('App/Models/Plantel')
class PlantelController {
  /**
   * Show a list of all plantels.
   * GET plantels
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
    const planteles = await Plantel.all()
    response.send(planteles)
  }

  /**
   * Create/save a new plantel.
   * POST plantels
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    const req = request.all()

    const plantel = await Plantel.create(req)
    response.send(plantel)
  }

  /**
   * Display a single plantel.
   * GET plantels/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
    const plantel = await Plantel.query().where('id', params.id).first()
    response.send(plantel)
  }

  /**
   * Update plantel details.
   * PUT or PATCH plantels/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    const req = request.all()
    const plantel = await Plantel.query().where('id', params.id).update(req)
    /* plantel.nombre = request.input('nombre')
    await plantel.save() */
    response.send(plantel)
  }

  /**
   * Delete a plantel with id.
   * DELETE plantels/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
    await Plantel.query().where('id', params.id).delete()
    response.send(true)
  }
}

module.exports = PlantelController
