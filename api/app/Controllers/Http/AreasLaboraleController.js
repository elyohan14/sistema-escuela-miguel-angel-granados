'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with areaslaborales
 */
const AreasLaborales = use('App/Models/AreasLaborale')
class AreasLaboraleController {
  /**
   * Show a list of all areaslaborales.
   * GET areaslaborales
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
    const areasLaborales = await AreasLaborales.all()
    response.send(areasLaborales)
  }

  /**
   * Create/save a new areaslaborale.
   * POST areaslaborales
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    const req = request.all()

    const areaLaboral = await AreasLaborales.create(req)
    response.send(areaLaboral)
  }

  /**
   * Display a single areaslaborale.
   * GET areaslaborales/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
    const areaLaboral = await AreasLaborales.query().where('id', params.id).first()
    response.send(areaLaboral)
  }

  /**
   * Update areaslaborale details.
   * PUT or PATCH areaslaborales/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    const areaLaboral = await AreasLaborales.query().where('id', params.id).first()
    areaLaboral.nombre = request.input('nombre')
    await areaLaboral.save()
    response.send(areaLaboral)
  }

  /**
   * Delete a areaslaborale with id.
   * DELETE areaslaborales/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
    await AreasLaborales.query().where('id', params.id).delete()
    response.send(true)
  }
}

module.exports = AreasLaboraleController
