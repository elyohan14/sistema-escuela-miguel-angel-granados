'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with estudiantes
 */
const Estudiante = use('App/Models/Estudiante')
class EstudianteController {
  /**
   * Show a list of all estudiantes.
   * GET estudiantes
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
    const estudiantes = await Estudiante.all()
    response.send(estudiantes)
  }

  /**
   * Create/save a new estudiante.
   * POST estudiantes
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    const data = request.only(['nombre', 'apellido', 'cedula', 'telefono', 'email'])
    const estudiante = await Estudiante.create(data)
    response.send(estudiante)
  }

  /**
   * Display a single estudiante.
   * GET estudiantes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
    const estudiante = await Estudiante.query().where('id', params.id).first()
    response.send(estudiante)
  }

  /**
   * Update estudiante details.
   * PUT or PATCH estudiantes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    const data = request.only(['nombre', 'apellido', 'cedula', 'telefono', 'email'])
    const estudiante = await Estudiante.find(params.id)
    estudiante.merge(data)
    await estudiante.save()
    response.send(estudiante)
  }

  /**
   * Delete a estudiante with id.
   * DELETE estudiantes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
    const estudiante = await Estudiante.find(params.id)
    await estudiante.delete()
    response.send(estudiante)
  }
}

module.exports = EstudianteController
