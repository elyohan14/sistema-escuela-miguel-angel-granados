'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with representantes
 */
const Representante = use('App/Models/Representante')
class RepresentanteController {
  /**
   * Show a list of all representantes.
   * GET representantes
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
    const representantes = await Representante.all()
    response.send(representantes)
  }

  /**
   * Create/save a new representante.
   * POST representantes
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    const data = request.only(['nombre', 'email', 'telefono', 'cedula', 'apellido', 'direccion'])
    const representante = await Representante.create(data)
    response.send(representante)
  }

  /**
   * Display a single representante.
   * GET representantes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
    const representante = await Representante.query().where('id', params.id).first()
    response.send(representante)
  }

  /**
   * Update representante details.
   * PUT or PATCH representantes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    const data = request.only([ 'nombre', 'apellido', 'cedula', 'telefono', 'email', 'direccion' ])
    const representante = await Representante.find(params.id)
    representante.merge(data)
    await representante.save()
    response.send(representante)
  }

  /**
   * Delete a representante with id.
   * DELETE representantes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
    const representante = await Representante.find(params.id)
    await representante.delete()
    response.send(representante)
  }
}

module.exports = RepresentanteController
