'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.get('/', () => {
  return { greeting: 'Hello world in JSON' }
})

const addPrefixToGroup = group => {
  // Grupo para rutas con prefijo /api/
  group.prefix("api");
  return group;
};

addPrefixToGroup(
  Route.group(() => {
    Route.get('empleados', 'EmpleadoController.index')
    Route.get('empleados/:id', 'EmpleadoController.show')
    Route.post('empleados', 'EmpleadoController.store')
    Route.put('empleados/:id', 'EmpleadoController.update')
    Route.delete('empleados/:id', 'EmpleadoController.destroy')

    Route.get('areas_laborales', 'AreasLaboraleController.index')
    Route.get('areas_laborales/:id', 'AreasLaboraleController.show')
    Route.post('areas_laborales', 'AreasLaboraleController.store')
    Route.put('areas_laborales/:id', 'AreasLaboraleController.update')
    Route.delete('areas_laborales/:id', 'AreasLaboraleController.destroy')

    Route.get('planteles', 'PlantelController.index')
    Route.get('planteles/:id', 'PlantelController.show')
    Route.post('planteles', 'PlantelController.store')
    Route.put('planteles/:id', 'PlantelController.update')
    Route.delete('planteles/:id', 'PlantelController.destroy')

    Route.get('estudiantes', 'EstudianteController.index')
    Route.get('estudiantes/:id', 'EstudianteController.show')
    Route.post('estudiantes', 'EstudianteController.store')
    Route.put('estudiantes/:id', 'EstudianteController.update')
    Route.delete('estudiantes/:id', 'EstudianteController.destroy')

    Route.get('representantes', 'RepresentanteController.index')
    Route.get('representantes/:id', 'RepresentanteController.show')
    Route.post('representantes', 'RepresentanteController.store')
    Route.put('representantes/:id', 'RepresentanteController.update')
    Route.delete('representantes/:id', 'RepresentanteController.destroy')

    Route.post('reporte1', 'EmpleadoController.imprimirReporte1')
  }))
