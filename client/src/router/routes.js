
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
      { path: 'empleados', component: () => import('pages/Empleados.vue') },
      { path: 'empleados/form', component: () => import('pages/EmpleadosForm.vue') },
      { path: 'empleados/form/:id', component: () => import('pages/EmpleadosForm.vue') },
      { path: 'planteles', component: () => import('pages/Planteles.vue') },
      { path: 'planteles/form', component: () => import('pages/PlantelesForm.vue') },
      { path: 'planteles/form/:id', component: () => import('pages/PlantelesForm.vue') },
      { path: 'areas_laborales', component: () => import('pages/AreasLaborales.vue') },
      { path: 'areas_laborales/form', component: () => import('pages/AreasLaboralesForm.vue') },
      { path: 'areas_laborales/form/:id', component: () => import('pages/AreasLaboralesForm.vue') },
      { path: 'estudiantes', component: () => import('pages/Estudiantes.vue') },
      { path: 'estudiantes/form', component: () => import('pages/EstudiantesForm.vue') },
      { path: 'estudiantes/form/:id', component: () => import('pages/EstudiantesForm.vue') },
      { path: 'representantes', component: () => import('pages/Representantes.vue') },
      { path: 'representantes/form', component: () => import('pages/RepresentantesForm.vue') },
      { path: 'representantes/form/:id', component: () => import('pages/RepresentantesForm.vue') },
      { path: 'annio_escolar', component: () => import('pages/AnnioEscolar.vue') }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
